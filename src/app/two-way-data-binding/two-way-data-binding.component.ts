import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-two-way-data-binding',
  templateUrl: './two-way-data-binding.component.html',
  styleUrls: ['./two-way-data-binding.component.scss']
})
export class TwoWayDataBindingComponent implements OnInit {

  nameOne:string;
  nameTwo:string;

  client = {
    firstName: "John",
    lastName: "Mack",
    address: "",
    age: 0
  };

  constructor() { }

  ngOnInit() {
  }

}
