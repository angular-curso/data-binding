import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.scss']
})
export class EventBindingComponent implements OnInit {

  i:number = 0;
  progress:number = 0;
  buttonName:string = "MyButton";
  spinnerMode:string = "determinate";
  btnEnable:boolean = true;
  selectDisabled:boolean = false;
  selectedOption:any;
  inputName:string = "Natan";

  constructor() { }

  ngOnInit() {
    this.hasSelectedOption();
  }

  save() {
    console.log("click");
  }

  inc() {
    this.i++;
    if (this.i <= 20) {
      this.progress = this.progress + 5;
    }
    this.buttonName = "It was clicked " + this.i + " times"
  }

  disable() {
    this.btnEnable = false;
    this.spinnerMode = "indeterminate";

    setTimeout(() => {
      this.btnEnable = true;
      this.spinnerMode = "determinate";
      this.progress = this.progress;
    }, 3000);
  }

  cbChange(event) {
    this.selectDisabled = event.checked;
  }

  selectionChange(event) {
    console.log(event);
    this.selectedOption = event.value;
  }

  hasSelectedOption() {
    if (!this.selectedOption) {
      this.selectedOption = "None";
    }
  }

}
