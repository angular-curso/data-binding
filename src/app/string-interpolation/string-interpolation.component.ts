import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-string-interpolation',
  templateUrl: './string-interpolation.component.html',
  styleUrls: ['./string-interpolation.component.scss']
})
export class StringInterpolationComponent implements OnInit {

  person = {
    firstName: "John",
    lastName: "Berke",
    age: 40,
    address: "Route 99"
  }

  constructor() { }

  ngOnInit() {
  }

}
